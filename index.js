const canvas = document.querySelector("canvas");
const ctx = canvas.getContext('2d');

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

class Ship {
    _speed = {
        x:0,
        y:0
    }

    constructor() {
        this._rotation = 0
        
        const image = new Image();
        image.src = "sprites/X-Wing.svg";
        image.onload = () => {
            this._image = image
            this._width = image.width
            this._height = image.height
            this._position = {
                x:(canvas.width /2) - 50,
                y:canvas.height - 120
            } 
        }
    }

    // Method pour intégrer l'image du "Ship"
    draw() {

        ctx.save()
        ctx.translate(
            player._position.x + player._width / 2,
            player._position.y + player._height / 2
        )
        ctx.rotate(this._rotation)

        ctx.translate(
            -player._position.x - player._width / 2,
            -player._position.y - player._height / 2
        )

        ctx.drawImage(
            this._image,
            this._position.x,
            this._position.y,
            this._width,
            this._height
        )

        ctx.restore()
    }
    // Method qui intègre la method "draw()", qui actualise la position du "Ship" selon son déplacement et pour empecher le "Ship" de sortir de l'écran.
    update() {
        if(this._image) {
            this.draw()
            this._position.x += this._speed.x
        }
    }
}

class Mob {
    _speed = {
        x:0,
        y:0
    }

    constructor({position}) {

        const image3 = new Image();
        image3.src = "sprites/Tie_fighter.svg";
        image3.onload = () => {
            this._image = image3
            this._width = image3.width * 0.5
            this._height = image3.height * 0.5
            this._position = {
                x: position.x,
                y: position.y
            }
        }
    }


    set lifeM(hit) {
        return this._life-= hit
    }
    // Method pour intégrer l'image de l'instance de "Mob"
    draw() {
        ctx.drawImage(
            this._image,
            this._position.x,
            this._position.y,
            this._width,
            this._height
        )
    }
    // Method qui intègre "draw()" et qui actualise la position de l'instance de "Mob" selon son déplacement.
    update({speed}) {
        if(this._image) {
            this.draw()
            this._position.x += speed.x
            this._position.y += speed.y
        }
    }
}

class Laser {
    constructor(imgSrc, {position,speed}) {
        this._speed = speed
        this._position = position
        this._radius = 3
        // this._direction = {
        //     x:0,
        //     y:0
        // }

        this._radius = 3

        const image1 = new Image();
        image1.src = imgSrc;
        image1.onload = () => {
            this._image = image1
            this._width = image1.width
            this._height = image1.height
        }
    }

    draw() {
        ctx.beginPath()

        ctx.drawImage(
            this._image,
            this._position.x - 36,
            this._position.y - 70,
            this._width * 1.2,
            this._height * 1.2
        )
        ctx.closePath()
    }

    update() {
        this.draw()
        this._position.x += this._speed.x
        this._position.y += this._speed.y
    }

    
}

class Missile {
    constructor({position,speed}) {
        this._speed = speed
        this._position = position
        this._radius = 3
        // this._direction = {
        //     x:0,
        //     y:0
        // }

        this._radius = 3

        const image2 = new Image();
        image2.src = "sprites/missile.svg";
        image2.onload = () => {
            this._image = image2
            this._width = image2.width
            this._height = image2.height
        }
    }

    draw() {
        ctx.beginPath()

        ctx.drawImage(
            this._image,
            this._position.x - 2,
            this._position.y,
            this._width * 1,
            this._height * 1
        )
        ctx.closePath()
    }

    update() {
        this.draw()
        this._position.x += this._speed.x
        this._position.y += this._speed.y
    }

    
}

class Grid {
    
    constructor() {
        this._position = {
            x:0,
            y:0
        }
        this._speed = {
            x:3,
            y:0
        }
        this._mobs = []
        const columns = Math.floor(Math.random() * 8  + 3)
        const rows = Math.floor(Math.random() * 3 + 2)

        this._width = columns * 100

        for (let x = 1; x < columns; x++) {
            for (let y = 1; y < rows; y++) {
                 this._mobs.push(
                    new Mob({
                        position: {
                            x:x * 100,
                            y:y * 100
                        }
                    })
                )
            }
        }
    }
    update() {
        this._position.x += this._speed.x
        this._position.y += this._speed.y
        
        this._speed.y = 0

        if(this._position.x + this._width >= canvas.width || this._position.x <= 0) {
            this._speed.x = -this._speed.x
            this._speed.y = 50
        }
    }
}
//  Création des instances.
const player = new Ship();
const grids = []
const lasers = []
const missiles = []

//  Variables pour mettre par défaut "false" pour les touches qui ont besoin de faire 2 actions différentes.
const keys = {
    ArrowLeft: {
        pressed: false
    },
    ArrowRight: {
        pressed: false
    }
}

let frames = 0

// Fonction pour mettre à jour constament les images. Elle se met à jour elle-même grâce à la method "requestAnimationFrame"
function animate() {
    window.requestAnimationFrame(animate)
    ctx.fillStyle = "black"
    ctx.fillRect(0, 0, canvas.width, canvas.height)
    player.update()

    lasers.forEach((laser, index) => {
        if(laser._position.y + laser._radius <= 0) {
            setTimeout(() => {
                lasers.splice(index, 1)
            }, 0)
            
        } else {
            laser.update()
        }
        
    })

    missiles.forEach((missile, index) => {
        if(missile._position.y + missile._radius <= 0) {
            setTimeout(() => {
                missiles.splice(index, 1)
            }, 0)
            
        } else {
            missile.update()
        }
        
    })

    grids.forEach(grid => {
        grid.update()
        grid._mobs.forEach(mob => {
            mob.update({speed: grid._speed})
        })
    })

    if(keys.ArrowLeft.pressed && player._position.x >= 20) {
    player._speed.x = -5
    player._rotation = -0.07
    } else if(
        keys.ArrowRight.pressed &&
        player._position.x + player._width + 20 <= canvas.width) {
            player._speed.x = 5
            player._rotation = 0.07
    } else{
        player._speed.x = 0
        player._rotation = 0
    }

    if(frames % 1000 === 0) {
        grids.push(new Grid())
    }
    frames++
}
// Appel de la fonction "animate()".
animate()

// Utilisation d'une method de EventTarget pour assigner une touche appuyée à un événement.
addEventListener('keydown', ({key}) => {
    switch(key) {
        case "ArrowLeft":
            keys.ArrowLeft.pressed = true
            break
    }
    switch(key) {
        case "ArrowRight":
            keys.ArrowRight.pressed = true
            break
    }
    switch(key) {
        case "Control" :
            missiles.push(new Missile({
                position: {
                    x:player._position.x ,
                    y:player._position.y - 100
                },
                speed: {
                    x:0,
                    y:-7
                }
            }))
            break
    }
    switch(key) {
        case " ":
            lasers.push(new Laser("sprites/laserG.png",{
                position: {
                    x:player._position.x + 22,
                    y:player._position.y - 50
                },
                speed: {
                    x:0,
                    y:-15
                }
            }))
            break
    }
    
} )

// Utilisation d'une method de EventTarget pour assigner une touche relachée à un événement.
addEventListener('keyup', ({key}) => {
    switch(key) {
        case "ArrowLeft":
            keys.ArrowLeft.pressed = false
            break
    }
    switch(key) {
        case "ArrowRight":
            keys.ArrowRight.pressed = false
            break
    }
    // switch(key) {
    //     case " ":
    //         keys.space.pressed= false
    //         break
    // }
    
} )
